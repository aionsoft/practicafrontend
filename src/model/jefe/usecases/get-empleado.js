import {traerEmpleadoApi} from "../jefe.repository";

const getEmpleado = (rut) =>{

    return new Promise((resolve, reject)=>{
        traerEmpleadoApi(rut).then(response=>{
            if(response.data.status){
                resolve(response.data.obj)
            }else{
                reject(response.data.code)
            }
        }).catch(error=>{
            reject(error)
        })
    })


}

export default getEmpleado;
