import {crearEmpleadoApi} from "../jefe.repository";

const crearEmpleado = (nombre,rut,sueldo,horaEntrada,horaSalida ) =>{

    return new Promise((resolve, reject)=>{
        crearEmpleadoApi(nombre,rut,sueldo,horaEntrada,horaSalida).then(response=>{
            if(response.data.status){
                resolve(response.data.obj)
            }else{
                reject(response.data.code)
            }
        }).catch(error=>{
            reject(error)
        })
    })


}

export default crearEmpleado;
