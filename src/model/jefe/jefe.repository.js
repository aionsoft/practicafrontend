import axios from "axios";
import qs from "qs";

export const traerEmpleadoApi = (rut) => {
    return new Promise((resolve, reject) => {
        axios.post(
            "/bienvenida?action=traer-empleado",
            qs.stringify({rut: rut})
        )
            .then(function (response) {
                resolve(response);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export const crearEmpleadoApi = (nombre,rut,sueldo,horaEntrada,horaSalida) => {
    return new Promise((resolve, reject) => {
        axios.post(
            "/bienvenida?action=crear-empleado",
            qs.stringify({
                rut: rut,
                nombre: nombre,
                sueldo:sueldo,
                horaEntrada:horaEntrada,
                horaSalida:horaSalida,
            })
        )
            .then(function (response) {
                resolve(response);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}