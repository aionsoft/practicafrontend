import getEmpleado from "@/model/jefe/usecases/get-empleado";
import crearEmpleado from "@/model/jefe/usecases/create-empleado";

const jefeEntity = {
    getEmpleado,
    crearEmpleado
}

export default jefeEntity;