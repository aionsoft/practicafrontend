import getGananciaEntreFechas from "@/model/empleado/usecases/get-ganancia-entre-fechas";
import createMarcarEntrada from "@/model/empleado/usecases/create-marcar-entrada";
import createMarcarSalida from "@/model/empleado/usecases/create-marcar-salida";
import obtenerRutEmpleado from "@/model/empleado/usecases/obtener-rut-empleado";
import guardarRutEmpleado from "@/model/empleado/usecases/guardar-rut-empleado";

const empleadoEntity = {
    getGananciaEntreFechas,createMarcarSalida,createMarcarEntrada, obtenerRutEmpleado, guardarRutEmpleado,
}

export default empleadoEntity;