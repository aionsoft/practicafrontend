import axios from "axios";
import qs from "qs";
import store from "@/store";

export const getGananciaEntreFechasApi = (rutEmpleado, fechaInicial, fechaFinal) => {
    return new Promise((resolve, reject) => {
        axios.post(
            "/bienvenida?action=traer-sueldo",
            qs.stringify({
                rut: rutEmpleado,
                fechaInicial: fechaInicial,
                fechaFinal: fechaFinal
            })
        )
            .then(function (response) {
                resolve(response);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}
export const createMarcarEntradaApi = (rutEmpleado) => {
    return new Promise((resolve, reject) => {
        axios.post(
            "/bienvenida?action=ingresar-entrada",
            qs.stringify({
                rut: rutEmpleado,
            })
        )
            .then(function (response) {
                resolve(response);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export const createMarcarSalidaApi = (rutEmpleado) => {
    return new Promise((resolve, reject) => {
        axios.post(
            "/bienvenida?action=ingresar-salida",
            qs.stringify({
                rut: rutEmpleado,
            })
        )
            .then(function (response) {
                resolve(response);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export const obtenerRutEmpleadoVuex = () => {
    return store.state.rutEmpleado;
};

export const guardarRutEmpleadoVuex = (rut) => {
    store.commit("setRutEmpleado", rut);
};

