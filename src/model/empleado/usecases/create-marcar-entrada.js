import {createMarcarEntradaApi} from "@/model/empleado/empleado.repository";

const createMarcarEntrada = (rutEmpleado) =>{

    return new Promise((resolve, reject)=>{
        createMarcarEntradaApi(rutEmpleado).then(response=>{
            if(response.data.status){
                resolve(response.data.obj)
            }else{
                reject(response.data.code)
            }
        }).catch(error=>{
            reject(error)
        })
    })

}

export default createMarcarEntrada;