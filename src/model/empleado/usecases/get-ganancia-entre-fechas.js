import {getGananciaEntreFechasApi} from "../empleado.repository";

const getGananciaEntreFechas = (rutEmpleado, fechaInicial, fechaFinal ) =>{
    let arraySeparador = String(fechaInicial).split("-");
    let cadenaSeparada = String(arraySeparador[2]) +"/"+ String(arraySeparador[1]) + "/"+ String(arraySeparador[0])
    fechaInicial=String(cadenaSeparada);

    arraySeparador = String(fechaFinal).split("-");
    cadenaSeparada = String(arraySeparador[2])+"/"+ String(arraySeparador[1]) +"/"+ String(arraySeparador[0])
    fechaFinal = String(cadenaSeparada)

    return new Promise((resolve, reject)=>{
        getGananciaEntreFechasApi(rutEmpleado, fechaInicial, fechaFinal).then(response=>{
            if(response.data.status){
                resolve(response.data.obj)
            }else{
                reject(response.data.code)
            }

        }).catch(error=>{
            reject(error)
        })
    })
}

export default getGananciaEntreFechas;
