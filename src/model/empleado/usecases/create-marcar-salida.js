import {createMarcarSalidaApi} from "@/model/empleado/empleado.repository";

const createMarcarSalida = (rutEmpleado) =>{

    return new Promise((resolve, reject)=>{
        createMarcarSalidaApi(rutEmpleado).then(response=>{
            if(response.data.status){
                resolve(response.data.obj)
            }else{
                reject(response.data.code)
            }
        }).catch(error=>{
            reject(error)
        })
    })

}

export default createMarcarSalida;