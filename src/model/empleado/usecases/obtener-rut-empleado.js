import {obtenerRutEmpleadoVuex} from "@/model/empleado/empleado.repository";

const obtenerRutEmpleado = () => {
    return obtenerRutEmpleadoVuex();
}

export default obtenerRutEmpleado;