import {guardarRutEmpleadoVuex} from "@/model/empleado/empleado.repository";

const guardarRutEmpleado = (rutEmpleado) => {
    guardarRutEmpleadoVuex(rutEmpleado);
}

export default guardarRutEmpleado;