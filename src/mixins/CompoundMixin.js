export default {
    data: function () {
        return {
            showDrawer: false,
        };
    },
    methods: {
        handleShowDrawer(){
            this.showDrawer = !this.showDrawer;
        }
    }
}