export default {
    methods: {
        showDrawer() {
            this.$emit("show-drawer");
        },
    }
}