import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [createPersistedState({
        storage: window.localStorage,
    })],
    state: {
        rutEmpleado: "17106797",
    },
    getters: {},
    mutations: {
        setRutEmpleado(state, rutEmpleado) {
            state.rutEmpleado = rutEmpleado;
        }
    },
    actions: {},
    modules: {}
})
