import Vue from 'vue'
import VueRouter from 'vue-router'
import MostrarSueldoIndex from "@/views/MostrarSueldo/MostrarSueldoIndex";
import TraerEmpleadoIndex from "@/views/TraerEmpleadoIndex/TraerEmpleadoIndex";
import IngresarEmpleadoIndex from "@/views/IngresarEmpleado/IngresarEmpleadoIndex";
import MarcarAsistenciaIndex from "@/views/MarcarAsistencia/MarcarAsistenciaIndex";
import MarcarSalidaIndex from "@/views/MarcarAsistencia/MarcarSalidaIndex";

Vue.use(VueRouter)

const routes = [

  {
    path: '/marcar-salida',
    name: 'holaMundo',
    component: MarcarSalidaIndex,
  },
  {
    path: '/ingresar-empleado',
    name: 'holaMundo',
    component: IngresarEmpleadoIndex,
  },
  {
    path: '/marcar-entrada',
    name: 'holaMundo',
    component: MarcarAsistenciaIndex,
  },
  {
    path: '/',
    name: 'holaMundo',
    component: MostrarSueldoIndex,
  },
  {
    path: '/traer-empleado',
    name: 'holaMundo',
    component: TraerEmpleadoIndex,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
